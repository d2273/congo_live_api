<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->float('rating');
            $table->integer('maximum');
            $table->enum('reason',['autograph','immo','taxis','request']);
            $table->string('uri')->nullable();
            $table->unsignedBigInteger('reviewer');
            $table->unsignedBigInteger('target');
            $table->foreign('reviewer')->references('id')->on('users');
            $table->foreign('target')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
