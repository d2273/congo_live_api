<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutographsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autographs', function (Blueprint $table) {
            $table->id();
            $table->float('amount')->nullable();
            $table->string('currency')->nullable();
            $table->enum('state',['pending','in_progress','finished','canceled','rejected']);
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->unsignedBigInteger('payment_id')->nullable();
            $table->unsignedBigInteger('fan_id');
            $table->unsignedBigInteger('celebrity_id');
            $table->foreign('fan_id')->references('id')->on('fans');
            $table->foreign('celebrity_id')->references('id')->on('celebrities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autographs');
    }
}
