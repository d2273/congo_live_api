<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string("poster")->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('video')->nullable();
            $table->string('audio')->nullable();
            $table->boolean('reviewable')->default(true);
            $table->unsignedBigInteger('celebrity_id');
            $table->foreign('celebrity_id')->references('id')->on('celebrities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
