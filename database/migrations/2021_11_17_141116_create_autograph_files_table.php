<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutographFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autograph_files', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type_mime');
            $table->string('file');
            $table->unsignedBigInteger('autograph_id');
            $table->foreign('autograph_id')->references('id')->on('autographs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autograph_files');
    }
}
