<?php

use App\Http\Controllers\Api\CashController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\DocController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/payment/{n}', [CashController::class, 'payment']);

Route::get('/payments',[CashController::class,'index']);
Route::get('/cash/accept',[CashController::class,'accept'])->name('cash.accept');
Route::get('/cash/cancel',[CashController::class,'cancel'])->name('cash.cancel');
Route::get('/cash/decline',[CashController::class,'decline'])->name('cash.decline');
Route::get('/cash/notify',[CashController::class,'notify'])->name('cash.notify');
