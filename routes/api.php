<?php

use App\Helpers\Treatment;
use App\Http\Controllers\Api\AccessController;
use App\Http\Controllers\Api\AutographController;
use App\Http\Controllers\Api\AutographFileController;
use App\Http\Controllers\Api\CashController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CelebrityCategoryController;
use App\Http\Controllers\Api\CelebrityController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\FanController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\ReviewController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('/users',UserController::class)->except(['store']);
    Route::post('/users/{id}',[UserController::class,'updatePoster']);
    Route::apiResource('/fans',FanController::class)->except(['store']);
    Route::apiResource('/celebrities',CelebrityController::class)->except(['store']);
    Route::apiResource('posts',PostController::class);
    Route::post('/posts/{id}',[PostController::class,'update']);
    Route::apiResource('comments',CommentController::class);
    Route::apiResource('autographs',AutographController::class);
    Route::apiResource('categories',CategoryController::class);
    Route::post('create/category',[CelebrityCategoryController::class,'store']);
    Route::apiResource('autograph_files',AutographFileController::class)->only('store');
    Route::apiResource('reviews',ReviewController::class);
});
Route::apiResource('/fans',FanController::class)->only(['store']);
Route::apiResource('/celebrities',CelebrityController::class)->only(['store']);
Route::post('login',[AccessController::class,'create']);
Route::get('/reference',function(){
    return response()->json(Treatment::getReference());
});
