<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Section extends Component
{
    public $fields=[];
    public $queries=[];
    public $id;
    public $example;
    public $title;
    public $url;
    public $verb;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($fields=[],$queries="",$id)
    {
        $this->fields=$fields;
        $r=$queries;
        $r=json_decode($queries);
        $this->example=$r->example;
        $this->title=$r->title;
        if(!empty($r->url)){
            $this->url=asset("$r->url");
        }
        if(!empty($r->verb)){
            $this->verb=$r->verb;
        }
        //var_dump($r->queries);die;
        $this->queries=$r->queries;
        $this->id=$id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.section');
    }
}
