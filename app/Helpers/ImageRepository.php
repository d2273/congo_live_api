<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Intervention\Image;
use Illuminate\Http\Request;
use App\Exceptions\Handler as Exception;
use App\Helpers\Treatment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Intervention\Image\ImageManagerStatic;

use function PHPUnit\Framework\fileExists;

class ImageRepository{


    /**
     * @return ['type','name','size','uri']|null
     */
    static function store(Request $request,$column,$folder){
        try {
          $status=null;
          $response=[];
          $val=15;
          $file=$request->file($column);
          if($file!=null){
              if($file->isFile()){
                  $extension=$file->extension();
                  $mime_type=$file->getMimeType();
                  $file_name=$file->getClientOriginalName();
                  $size=$file->getSize();
                  $key=Treatment::getImageKey();
                  $name=$key.'.'.$extension;
                  $image=$request->file($column);
                  $rep=$image->storeAs($folder,$name,'public');
                  $response['type']=$mime_type;
                  $response['name']=$file_name;
                  $response['size']=$size;
                  $response['uri']=$rep;
              }
          }
        } catch (Exception $e) {

        }
        return $response;

      }

          /**
     * @return ['type','name','size']|null
     */
    static function getInfo(Request $request,$column,$folder){
        try {
          $status=null;
          $response=[];
          $val=15;
          $file=$request->file($column);
          if($file!=null){
              if($file->isFile()){
                  $extension=$file->extension();
                  $mime_type=$file->getMimeType();
                  $file_name=$file->getClientOriginalName();
                  $size=$file->getSize();
                  $key=Treatment::getImageKey();
                  $name=$key.'.'.$extension;
                  $image=$request->file($column);
                  $response['type']=$mime_type;
                  $response['name']=$file_name;
                  $response['size']=$size;
              }
          }
        } catch (Exception $e) {

        }
        return $response;

      }

    public function createThumb($path){
        try {
            /*$img=ImageManagerStatic::make($path);
            $img->resize(240,240);
            $img->save($path);*/
        } catch (Exception $th) {
            //throw $th;
        }
    }

    public function createThumbSize($path,$width,$height){
        try {
            /*$img=ImageManagerStatic::make($path);
            $img->resize($width,$height);
            $img->save($path);*/
        } catch (Exception $th) {
            //throw $th;
        }
    }

    static function saveFileOnFirebase(Request $request,Model $model,$folder="file",$column){
        $status=false;
        $info=ImageRepository::getInfo($request,$column,"");
        try {
            $data=$request->file($column);
            $pathName=$data->getPathname();
            $file=fopen($pathName,'r');
            $extension=$data->getClientOriginalExtension();
            $key=Treatment::getPrivateImage($model::all(),"$folder","$folder/",".".$extension);
            $firebase=new FirebaseService();
            $firebase->setStorage($file,$key);
            $info['key']=$key;
            $status=true;
        } catch (\Throwable $th) {

        }
        $info['status']=$status;
        return $info;
    }

}
