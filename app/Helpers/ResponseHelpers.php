<?php
namespace App\Helpers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ResponseHelpers {

    static function getValidationResponse(Validator $validation){
        return response()->json(['error'=>$validation->getMessageBag()],403);
    }

    /**
     * @return array
     */
    static function getFillableData(Request $request,Model $model):array {
        $data=[];
        $fillable=$model->getFillable();
        $items=$request->all();
        foreach ($items as $key => $value) {
            if(in_array($key,$fillable)){
                $data[$key]=$value;
            }
        }
        return $data;
    }
}

