<?php
namespace App\Helpers;

use App\Models\User;
use Kreait\Firebase\Factory;

class FirebaseService{

    private $factory=null;
    public function __construct()
    {
        $this->factory = (new Factory)
                ->withServiceAccount(__DIR__.'/firebase.json')
                ->withDatabaseUri('https://congo-live-default-rtdb.asia-southeast1.firebasedatabase.app/');;
    }

    /**
     * Undocumented function
     *
     * @param User USer
     * @param User User
     * @param [type] $type
     * @param string $column
     * @param string $message
     * @param string $title
     * @return void
     */
    public function createNotification(User $sender,User $receiver,$type,$column="id",$message="",$title=""){
        $factory=$this->factory;
        $database=$this->factory->createDatabase();
        $database->getReference("users/user_$receiver[$column]")
            ->push([
                "type"=>$type,
                "sender"=>$sender[$column],
                "message"=>$message,
                "title"=>$title,
                "readed"=>false,
        ]);

    }

    public function setStorage($file,$name){
        $storage = $this->factory->createStorage();
        $bucket=$storage->getBucket();
        $object=$bucket->upload($file,[
            'name' => $name
        ]);
        return $object;
    }

    public function getUrl($file){
        if($file==null){
            return null;
        }
        $expire_at=new \DateTime('tomorrow');
        $storage=$this->factory->createStorage();
        $url=$storage->getBucket()->object($file)->signedUrl($expire_at);
        return $url;
    }
}
