<?php
namespace App\Helpers;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

use function PHPUnit\Framework\isEmpty;
use Illuminate\Support\Str;

class Treatment{

    public static function getPrivateKey(Collection $list,$column){
        $passer=true;
        $key='';
        do{
            $passer=true;
            $key=strtolower(Str::random(64));
            $rep=$list->where($column,$key);
            if(!isEmpty($rep)){
                $passer=false;
            }
        }while(!$passer);

        return $key;
    }

    public static function getPrivateImage(Collection $list,$column="",$folder="",$extension=""){
        $passer=true;
        $key='';
        do{
            $passer=true;
            $key=$folder.strtolower(Str::random(12)).$extension;
            $rep=$list->where($column,$key);
            if(!isEmpty($rep)){
                $passer=false;
            }
        }while(!$passer);

        return $key;
    }

    public static function getImageKey(){
        $key=Str::random(60);
        return $key;
    }
    public static function getPassword(){
        $key=Str::random(8);
        return $key;
    }

    public static function getReference(){
        $year=date('y');
        $month=date('m');
        $day=date('d');
        $minute=date('i');
        $second=date('s');
        $hour=date('h');
        $ooo='444';
        $oo="44";
        $o="4";
        $response=$year.$ooo.$hour.$month.$minute.$oo.$day.$second.$o;
        return $response;
    }
}
