<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Celebrity extends Model
{

    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function relation(){
        return $this->belongsToMany(Category::class,'celebrity_categories');
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function autographs(){
        return $this->hasMany(Autograph::class);
    }


}
