<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CelebrityCategory extends Model
{
    use HasFactory;

    public function celebrities(){
        return $this->hasMany(Celebrity::class);
    }

    protected $fillable=[
        'enabled'
    ];
}
