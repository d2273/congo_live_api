<?php

namespace App\Models;

use App\Http\Controllers\Api\CelebrityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title','content','poster',
        'thumbnail','video','audio',
        'reviewable'
    ];

    public function celebrity(){
        return $this->belongsTo(Celebrity::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
