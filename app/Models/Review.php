<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $fillable=[
      'rating','maximum','reason','uri'
    ];

    public function reviewer(){
        return $this->belongsTo(User::class,'reviewer');
    }

    public function target(){
        return $this->belongsTo(User::class,'target');
    }
}
