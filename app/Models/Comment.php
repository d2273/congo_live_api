<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'content'
    ];

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function fan(){
        return $this->belongsTo(Fan::class);
    }

}
