<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autograph extends Model
{
    use HasFactory;

    public function files(){
        return $this->hasMany(AutographFile::class);
    }

    public function fan(){
        return $this->belongsTo(Fan::class);
    }

    public function celebrity(){
        return $this->belongsTo(Celebrity::class);
    }

    protected $fillable=[
        'amount','description','currency','payment_id','state','type','event_date'
    ];
}
