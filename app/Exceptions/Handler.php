<?php

namespace App\Exceptions;

use ArgumentCountError;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use InvalidArgumentException;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Google\Cloud\Core\Exception\NotFoundException;
use Kreait\Firebase\Exception\Database\DatabaseNotFound;
use RuntimeException;
use Swift_TransportException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request,Throwable $e)
    {
        if($e instanceof ModelNotFoundException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        /*if($e instanceof BindingResolutionException){
            return response()->json(['error'=>'Binding'],404);
        }*/
        if($e instanceof QueryException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof MassAssignmentException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof Swift_TransportException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof MethodNotAllowedHttpException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof RuntimeException){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof RouteNotFoundException){
            return response()->json(['error'=>"You are not allowed to use this route"],401);
        }
        if($e instanceof ArgumentCountError){
            return response()->json(['error'=>$e->getMessage()],400);
        }
        if($e instanceof InvalidArgumentException){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        if($e instanceof NotFoundException){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        if($e instanceof DatabaseNotFound){
            return response()->json(['error'=>$e->getMessage()],404);
        }
        return parent::render($request,$e);
    }
}
