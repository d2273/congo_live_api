<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocController extends Controller
{

    public function index(){
        $data=[];
        $data=array_merge($data,$this->getLogin());
        $data=array_merge($data,$this->getCreateCustomer());
        $data=array_merge($data,$this->retrieveCustomer());

        $data=array_merge($data,$this->getCreateArtist());
        $data=array_merge($data,$this->retrieveArtist());

        $data=array_merge($data,$this->getCreateCategory());
        $data=array_merge($data,$this->getAllCategories());
        $data=array_merge($data,$this->retrieveCategory());

        $data=array_merge($data,$this->getCreateRequest());
        $data=array_merge($data,$this->getAllRequests());

        $data=array_merge($data,$this->getCreatePost());
        $data=array_merge($data,$this->getAllPosts());
        $data=array_merge($data,$this->retrievePost());
        //$data=array_merge($data,$this->getCreateComment());
        $data=collect($data);
        return view('doc',['data'=>$data]);
    }

    private function getLogin(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    )
                ),
                "title"=>"Login",
                "id"=>'content-get-login',
                "example"=>'{
                    "id": 4,
                    "username": null,
                    "first_name": "Joseph",
                    "last_name": "Mbala",
                    "phone": "+24381662496",
                    "photo": null,
                    "enabled": 1,
                    "civility": "m",
                    "email": "jo.mbala@gmail.com",
                    "email_verified_at": null,
                    "created_at": "2021-11-08T04:54:04.000000Z",
                    "updated_at": "2021-11-08T04:54:04.000000Z",
                    "token": "4|u1RlTF5ZgpjgktqUkLtRVTKuAnqwT7TnYihi4kpA",
                    "customer": {
                      "id": 1,
                      "user_id": 4,
                      "created_at": "2021-11-08T04:54:04.000000Z",
                      "updated_at": "2021-11-08T04:54:04.000000Z"
                    },
                    "artist": null
                  }
                  the user can be artist or customer
                  ',
                "url"=>"api/login",
                'verb'=>'POST'
            ]
        ];
        return $item;
    }

    private function getCreateCustomer(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number is required and unique"
                    ),
                    array(
                        "field"=>"email",
                        "type"=>"string",
                        "description"=>"(optional) email address, unique"
                    ),
                    array(
                        "field"=>"first_name",
                        "type"=>"string",
                        "description"=>"user first name"
                    ),
                    array(
                        "field"=>"last_name",
                        "type"=>"string",
                        "description"=>"user last name"
                    ),
                    array(
                        "field"=>"photo",
                        "type"=>"image",
                        "description"=>"(optional) the photo file"
                    ),
                    array(
                        "field"=>"civility",
                        "type"=>"string",
                        "description"=>"(optional) m for man and mme for woman"
                    ),
                    array(
                        "field"=>"username",
                        "type"=>"string",
                        "description"=>"(optional) user name is unique"
                    )
                ),
                "title"=>"Create customer",
                "id"=>'content-create-customer',
                "example"=>'{
                    "email": "henock@gmail.com",
                    "first_name": "henock",
                    "last_name": "massamba",
                    "phone": "+243823995810",
                    "civility": "m",
                    "enabled": true,
                    "username": null,
                    "updated_at": "2021-11-07T22:48:15.000000Z",
                    "created_at": "2021-11-07T22:48:15.000000Z",
                    "id": 2,
                    "token": "2|fScms7mxws3Cj82Exuae0MNV4ppIlhX7QJNUrfOC",
                    "user": 2
                }
                Use the token to authentificate the current user',
                'url'=>'api/customers',
                'verb'=>'POST'
            ]
        ];
        return $item;
    }

    private function getCreateArtist(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number is required and unique"
                    ),
                    array(
                        "field"=>"email",
                        "type"=>"string",
                        "description"=>"(optional) email address, unique"
                    ),
                    array(
                        "field"=>"first_name",
                        "type"=>"string",
                        "description"=>"user first name"
                    ),
                    array(
                        "field"=>"last_name",
                        "type"=>"string",
                        "description"=>"user last name"
                    ),
                    array(
                        "field"=>"photo",
                        "type"=>"image",
                        "description"=>"(optional) the photo file"
                    ),
                    array(
                        "field"=>"civility",
                        "type"=>"string",
                        "description"=>"(optional) m for man and mme for woman"
                    ),
                    array(
                        "field"=>"username",
                        "type"=>"string",
                        "description"=>"(optional) user name is unique"
                    )
                ),
                "title"=>"Create artist",
                "id"=>'content-create-artist',
                "example"=>'{
                    "email": "danielkamen@gmail.com",
                    "first_name": "Daniel",
                    "last_name": "kameni",
                    "phone": "+243810027967",
                    "civility": "m",
                    "enabled": true,
                    "username": null,
                    "updated_at": "2021-11-08T04:34:55.000000Z",
                    "created_at": "2021-11-08T04:34:55.000000Z",
                    "id": 1,
                    "token": "1|01dgb2s3BHTujeTvLK21PRgKoE32aN4RphM5i8zu",
                    "user": 3
                  }',
                'verb'=>'POST',
                'url'=>'api/artists'
            ]
        ];
        return $item;
    }

    private function getCreateCategory(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"title",
                        "type"=>"string",
                        "description"=>"The title is required"
                    ),
                    array(
                        "field"=>"description",
                        "type"=>"string",
                        "description"=>"(optional) category description"
                    ),
                ),
                "title"=>"Create category",
                "id"=>'content-create-category',
                "example"=>'{
                    "title": "évangéliste",
                    "description": null,
                    "updated_at": "2021-11-07T23:02:38.000000Z",
                    "created_at": "2021-11-07T23:02:38.000000Z",
                    "id": 1
                  }
                ',
                'verb'=>'POST',
                'url'=>'api/categories'
            ]
        ];
        return $item;
    }

    private function getCreateRequest(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"artist",
                        "type"=>"int",
                        "description"=>"the id artist"
                    ),
                    array(
                        "field"=>"customer",
                        "type"=>"int",
                        "description"=>"the id customer"
                    ),
                    array(
                        "field"=>"amount",
                        "type"=>"float",
                        "description"=>"(optional) amount than customer can paid"
                    ),
                    array(
                        "field"=>"currency",
                        "type"=>"string",
                        "description"=>"(optional) $, euro, yen,..."
                    ),
                    array(
                        "field"=>"description",
                        "type"=>"string",
                        "description"=>"(optional)"
                    ),
                ),
                "title"=>"Create request",
                "id"=>'content-create-request',
                "example"=>'{
                    "customer_id": 1,
                    "artist_id": 1,
                    "amount": null,
                    "currency": null,
                    "description": "Je vous propose de me dédicacer dans votre prochain album",
                    "state": "in_progress",
                    "updated_at": "2021-11-08T04:55:25.000000Z",
                    "created_at": "2021-11-08T04:55:25.000000Z",
                    "id": 2
                  }',
                'verb'=>'POST',
                'url'=>'api/requests'
            ]
        ];
        return $item;
    }

    private function getCreatePost(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"title",
                        "type"=>"string",
                        "description"=>"The title is required"
                    ),
                    array(
                        "field"=>"artist",
                        "type"=>"int",
                        "description"=>"The id artist , this id must exists"
                    ),
                    array(
                        "field"=>"description",
                        "type"=>"string",
                        "description"=>"(optional) category description"
                    ),
                ),
                "title"=>"Create post",
                "id"=>'content-create-post',
                "example"=>'
                    {
                        "artist_id": 1,
                        "title": "sorie officiel de l\'album",
                        "content": "jdjsdjk lorem ipsum sh",
                        "updated_at": "2021-11-08T04:40:42.000000Z",
                        "created_at": "2021-11-08T04:40:42.000000Z",
                        "id": 2
                    }
                ',
                "verb"=>"POST",
                "url"=>"api/posts"
            ]
        ];
        return $item;
    }

    private function getCreateComment(){
        $item=[
            [
                "queries"=>array(
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    ),
                    array(
                        "field"=>"email",
                        "type"=>"string",
                        "description"=>"(optional) email address"
                    ),
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    ),
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    ),
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    ),
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    ),
                    array(
                        "field"=>"phone",
                        "type"=>"string",
                        "description"=>"phone number maximum 15 lines"
                    )
                ),
                "title"=>"Create comment",
                "id"=>'content-create-comment',
                "example"=>""
            ]
        ];
        return $item;
    }

    private function getAllCategories(){
        $item=[
            [
                "queries"=>array(

                ),
                "title"=>"All categories",
                "id"=>'content-all-category',
                "example"=>'[
                    {
                      "id": 1,
                      "title": "Religion",
                      "description": "Je vous propose de me dédicacer dans votre prochain album",
                      "created_at": "2021-11-08T05:14:53.000000Z",
                      "updated_at": "2021-11-08T05:14:53.000000Z"
                    },
                    {
                      "id": 2,
                      "title": "Comédien",
                      "description": null,
                      "created_at": "2021-11-08T05:15:25.000000Z",
                      "updated_at": "2021-11-08T05:15:25.000000Z"
                    }
                  ]',
                'verb'=>'GET',
                'url'=>'api/categories'
            ]
        ];
        return $item;
    }

    private function getAllPosts(){
        $item=[
            [
                "queries"=>array(

                ),
                "title"=>"All posts",
                "id"=>'content-all-post',
                "example"=>'',
                'verb'=>'GET',
                'url'=>'api/posts'
            ]
        ];
        return $item;
    }

    private function getAllRequests(){
        $item=[
            [
                "queries"=>array(

                ),
                "title"=>"All requests",
                "id"=>'content-all-request',
                "example"=>'',
                'verb'=>'GET',
                'url'=>'api/requests'
            ]
        ];
        return $item;
    }

    private function retrieveArtist(){
        $item=[
            [
                "queries"=>array(
                    array(
                        'field'=>'id',
                        'type'=>'int',
                        'description'=>'The artist id'
                    )
                ),
                "title"=>"Retrieve artist",
                "id"=>'content-retrieve-artist',
                "example"=>'',
                'verb'=>'GET',
                'url'=>'api/artists/<id>'
            ]
        ];
        return $item;
    }

    private function retrieveCustomer(){
        $item=[
            [
                "queries"=>array(
                    array(
                        'field'=>'id',
                        'type'=>'int',
                        'description'=>'The customer id'
                    )
                ),
                "title"=>"Retrieve customer",
                "id"=>'content-retrieve-customer',
                "example"=>'',
                'verb'=>'GET',
                'url'=>'api/customers/<id>'
            ]
        ];
        return $item;
    }

    private function retrievePost(){
        $item=[
            [
                "queries"=>array(
                    array(
                        'field'=>'id',
                        'type'=>'int',
                        'description'=>'The post id'
                    )
                ),
                "title"=>"Retrieve post",
                "id"=>'content-retrieve-post',
                "example"=>'',
                'verb'=>'GET',
                'url'=>'api/posts/<id>'
            ]
        ];
        return $item;
    }

    private function retrieveCategory(){
        $item=[
            [
                "queries"=>array(
                    array(
                        'field'=>'id',
                        'type'=>'int',
                        'description'=>'The category id'
                    )
                ),
                "title"=>"Retrieve category",
                "id"=>'content-retrieve-category',
                "example"=>'{
                    "id": 2,
                    "title": "Comédien",
                    "description": null,
                    "created_at": "2021-11-08T05:15:25.000000Z",
                    "updated_at": "2021-11-08T05:15:25.000000Z"
                  }',
                'verb'=>'GET',
                'url'=>'api/categories/<id>'
            ]
        ];
        return $item;
    }

}
