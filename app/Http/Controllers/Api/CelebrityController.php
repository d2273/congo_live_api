<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Celebrity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CelebrityController extends Controller
{



    /**
     * CELEBRITY: Display a listing of the celebrity.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $list=Celebrity::all();
        $availlables=array(
            'category.name',
            'category_id'
        );
        $items=[];
        foreach ($list as $value) {
            $model=$this->getModel($value);
            $items[]=$model;
        }
        $test=0;
        /**
         * Filter by category name
         */
        if($request->category){
            $items = collect($items);
            foreach ($items as $key => $value) {
                $test = $value->categories->where('title',$request->category);
                if($test->count() === 0){
                    $items->forget($key);
                }
            }
        }
        /**
         * Filter by category id
         */
        if($request->category_id){
            $items = collect($items);
            foreach ($items as $key => $value) {
                $test = $value->categories->where('id',$request->category_id);
                if($test->count() === 0){
                    $items->forget($key);
                }
            }
        }
        return response()->json($items);
    }

    /**
     * CELEBRITY: Record celebrity information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'unique:users,phone',
            'username'=>'unique:users,username',
            'photo'=>'image',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:8',
            'civility'=>['required',Rule::in(['m','mme'])]
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $userController=new UserController();
        $user=$userController->store($request);
        $user=$user->original;
        $model=new Celebrity();
        $model->user_id=$user->id;
        $model->save();
        $user->user=$user->id;
        $user->id=$model->id;
        return response()->json($user);
    }

    /**
     * CELEBRITY: Display the specified celebrity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Celebrity::find($id);
        if($item!=null){
            $model=$this->getModel($item);
            return response()->json($model);
        }else{
            return response()->json($item,404);
        }
    }

    /**
     * CELEBRITY: Update the specified celebrity in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * CELEBRITY: Remove the specified celebrity from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getModel(Celebrity $item){
        $item->user;
        $item->categories;
        $item->posts;
        $item->autographs;
        $model=$this->getCelebrity($item);
        $model->review=$this->getReview($item->user->reviews);
        $model->posts=$item->posts->count();
        $model->autographs=$item->autographs()->where('state','in_progress')->get();
        $model->makeHidden(['reviews']);

        return $model;
    }

    static function getCelebrity(Celebrity $item){
        $celebrity=null;
        $celebrity=$item->user;
        if(null!=$celebrity->photo){
            $firebase =new FirebaseService();
            $celebrity->photo=$firebase->getUrl($celebrity->photo);
        }
        $celebrity->user=$celebrity->id;
        $celebrity->id=$item->id;
        $celebrity->categories=$item->relation;
        return $celebrity;
    }

    private function getReview($list=[]){
        $response=0;
        $som=0;
        $result=0;
        foreach ($list as $value) {
            $som+=$value['maximum'];
            $result+=$value['rating'];
        }
        if($som>0){
            $response=($result/$som)*100;
        }
        return $response;
    }
}
