<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Enum;
use App\Helpers\Treatment;
use App\Http\Controllers\Controller;
use App\Models\Autograph;
use Illuminate\Http\Request;

class CashController extends Controller
{
    public function index(Request $request){
        $reference=Treatment::getReference();
        $merchant="1f134f9556e54d1893adb6901f4a410c";
        $password="8e397cc441ab4395b444aed771dae3be";
        $auth_phone="";
        $auth_email="";
        $amount=5000;
        return view('payment',compact('reference','amount','merchant','password','auth_phone','auth_email'));
    }
    public function payment($id)
    {
        $item=Autograph::find($id);
        if($item===null){
            return "not found";
        }
        $model=new AutographController();
        $item=$model->getModel($item);
        $reference=Treatment::getReference();
        if(null==$item->amount && 'finished'!==$item->state){
            //cancel process
        }
        return view('app', compact('item', 'reference'));
    }
    public function accept()
    {
        return response()->json('cool',201);
    }

    public function cancel()
    {
        return response()->json('cancel',403);
    }

    public function decline()
    {
        return response()->json('decline',400);
    }

    public function notify()
    {
        return response()->json('notify');
    }
}
