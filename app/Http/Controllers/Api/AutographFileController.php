<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ImageRepository;
use App\Helpers\ResponseHelpers;
use App\Helpers\Treatment;
use App\Http\Controllers\Controller;
use App\Models\AutographFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AutographFileController extends Controller
{
    /**
     * AUTOGRPH FILE: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * AUTOGRPH FILE: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'file'=>'required|file',
            'autograph'=>'required|exists:autographs,id'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        //$file=ImageRepository::store($request,'file',"autographs");
        $info=ImageRepository::getInfo($request,'file',"autographs");
        $data=$request->file('file');
        $pathName=$data->getPathname();
        $file=fopen($pathName,'r');
        $extension=$data->getClientOriginalExtension();
        $key=Treatment::getPrivateImage(AutographFile::all(),"file","file/",".".$extension);
        $firebase=new FirebaseService();
        $firebase->setStorage($file,$key);
        $model=new AutographFile();
        $model->name=$info['name'];//$file['name'];
        $model->type_mime=$info['type'];//$file['type'];
        $model->file=$key;//$file['uri'];
        $model->autograph_id=$request->autograph;
        $model->save();
        return response()->json($model);
    }

    /**
     * AUTOGRPH FILE: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * AUTOGRPH FILE: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * AUTOGRPH FILE: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
