<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Enum;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ReviewController extends Controller
{
    /**
     * REVIEW: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Review::all();
        return response()->json($list);
    }

    /**
     * REVIEW: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'rating'=>'required|numeric',
            'maximum'=>'required|numeric',
            'target'=>'required|exists:users,id',
            'reviewer'=>'required|exists:users,id||different:target',
            'reason'=>['required',Rule::in(Enum::REVIEW_REASON())],
            'endpoint'=>[Rule::in(['autographs'])],
            'index'=>'numeric'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if($request->rating>$request->maximum){
            return response()->json("The rate can't be more than maximum point",400);
        }
        $model=new Review();
        $model->rating=$request->rating;
        $model->maximum=$request->maximum;
        $model->reason=$request->reason;
        if($request->endpoint && $request->index){
            $model->uri=$request->endpoint.'/'.$request->index;
        }
        $model->reviewer=$request->reviewer;
        $model->target=$request->target;
        $model->save();
        return response()->json($model);
    }

    /**
     * REVIEW: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model=Review::find($id);
        $model->reviewer;
        $model->target;
        return response()->json($model);
    }

    /**
     * REVIEW: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * REVIEW: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
