<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ImageRepository;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\CelebrityCategory;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * POST: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Post::all();
        $items=[];
        foreach ($list as $value) {
            $items[]=$this->getModel($value);
        }
        return response()->json($items);
    }

    /**
     * POST: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'celebrity'=>'required|exists:celebrities,id',
            'title'=>'required',
            'content'=>'required',
            'poster'=>'image',
            'video'=>'file|mimes:mp4,mov,ogg,qt,webm',
            'audio'=>'file|mimes:mp3'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $model=new Post();
        $model->celebrity_id=$request->celebrity;
        $model->title=$request->title;
        $model->content=$request->content;
        if($request->poster){
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"poster","poster");
            //$image=ImageRepository::store($request,"poster","posters");
            if($file['status']===true){
                $model->poster=$file['key'];
            }
        }
        if($request->video){
            //$file=ImageRepository::store($request,"video","videos");
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"videos","video");
            if($file['status']===true){
                $model->video=$file['key'];
            }
        }
        if($request->audio){
            //$file=ImageRepository::store($request,"audio","audios");
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"audios","audio");
            if($file['status']===true){
                $model->audio=$file['key'];
            }
        }
        $model->reviewable=true;
        $model->save();
        return response()->json($model);
    }

    /**
     * POST: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Post::find($id);
        if($item!=null){
            $model=$this->getModel($item);
            return response()->json($model);
        }else{
            return response()->json($item,404);
        }
    }

    /**
     * POST: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new Post());
        $validation=Validator::make($data,[
            'title'=>'min:1',
            'content'=>'min:1',
            'poster'=>'image',
            'video'=>'file|mimes:mp4,mov,ogg,qt,webm',
            'audio'=>'file|mimes:mp3',
            'reviewable'=>'boolean'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        $model=Post::find($id);
        if($model==null){
            return response()->json($model,404);
        }
        if($request->poster){
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"poster","poster");
            //$image=ImageRepository::store($request,"poster","posters");
            if($file['status']===true){
                $data['poster']=$file['key'];
            }
        }
        if($request->video){
            //$file=ImageRepository::store($request,"video","videos");
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"videos","video");
            if($file['status']===true){
                $data['video']=$file['key'];
            }
        }
        if($request->audio){
            //$file=ImageRepository::store($request,"audio","audios");
            $file=ImageRepository::saveFileOnFirebase($request,new Post(),"audios","audio");
            if($file['status']===true){
                $data['audio']=$file['key'];
            }
        }
        $model->update($data);
        return response()->json($model);
    }

    /**
     * POST: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getModel(Post $post){
        $item=$post;
        $celebrity=new CelebrityController();
        $author=$celebrity->show($item->celebrity_id);
        $firebase=new FirebaseService();
        if($item->poster!=null){
            $item->poster=$firebase->getUrl($item->poster);
        }
        if($item->video!=null){
            $item->video=$firebase->getUrl($item->video);
        }
        if($item->audio!=null){
            $item->audio=$firebase->getUrl($item->audio);
        }
        $item->comments=$item->comments()->count();
        $item->celebrity=$author->original;
        return $item;
    }
}
