<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Fan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class FanController extends Controller
{
    /**
     * FAN: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Fan::all();
        $items=[];
        foreach ($list as $value) {
            $model=$this->getModel($value);
            $items[]=$model;
        }
        return response()->json($items);
    }

    /**
     * FAN: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'unique:users,phone',
            'username'=>'unique:users,username',
            'photo'=>'image',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:8',
            'civility'=>['required',Rule::in(['m','mme'])]
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $userController=new UserController();
        $user=$userController->store($request);
        $user=$user->original;
        $model=new Fan();
        $model->user_id=$user->id;
        $model->save();
        $user->user=$user->id;
        $user->id=$model->id;
        return response()->json($user);
    }

    /**
     * FAN: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Fan::find($id);
        if($item!=null){
            $model=$this->getModel($item);
            return response()->json($model);
        }else{
            return response()->json($item,404);
        }
    }

    /**
     * FAN: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * FAN: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getModel(Fan $item){
        $item->user;
        $item->autographs;
        $model=$this->getFan($item);
        $model->reviews=$item->user->reviews;
        $model->autographs=$item->autographs;
        return $model;
    }

    static function getFan(Fan $item){
        $fan=null;
        $fan=$item->user;
        if(null!=$fan->photo){
            $firebase =new FirebaseService();
            $fan->photo=$firebase->getUrl($fan->photo);
        }
        $fan->user=$fan->id;
        $fan->id=$item->id;
        return $fan;
    }
}
