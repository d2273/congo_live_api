<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ImageRepository;
use App\Helpers\ResponseHelpers;
use App\Helpers\Treatment;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * USER: Display all users.
     *
     *     /**
     * USER: Display the user by id.
     *
     * @response [{
     * "id": 1,
     * "username": null,
     * "first_name": "henock",
     * "last_name": "massamba",
     * "photo": "https://storage.googleapis.com/congo-live.appspot.com/profil/font8zqejiuo.png?GoogleAccessId=firebase-adminsdk-s7bgq%40congo-live.iam.gserviceaccount.com&Expires=1646870400&Signature=FUNE4CN7CTzt0FuLxMW3r0yUR0YK3ZirQ2wTjWPvZ%2B1iwSqGmEHL9xDgMUCsuaftsV1RjJO2mD9Tq9JWc585lKvY1z8ji8y719E21vZ98imYykwEck3MQzktMEoDR7NDfu8QpGgAA70NF1eHpcd8uIzPBr9zzrK3yiV%2FFG4%2FrQ%2FUBQrhAIJt34uptBUe2lWdqEZjwCF3gTsW0dCfZEvyzeDoTZ05QaXF3xpscAGIvYAKR2Ddc1bxiqYDmGSrVQjvak8DGSO6DhE2TeLLoJDBEVQsuNW3cwIjYKW9ymwqGzyD48w%2Fb23HjAhHRjSIgNwPwQZQWdKlzR7WIiTqmIiDuQ%3D%3D",
     * "enabled": 1,
     * "civility": "m",
     * "email": null,
     * "email_verified_at": null,
     * "address": null,
     * "site_web": null,
     * "thumbnail": null,
     * "created_at": "2022-01-30T07:07:50.000000Z",
     * "updated_at": "2022-01-30T08:29:24.000000Z",
     * "phone": null,
     * "user": 1,
     * "categories": [],
     * "review": 0,
     * "posts": 18,
     * "autographs": [
     *{
    * "id": 1,
    * "amount": null,
    * "currency": null,
    * "state": "in_progress",
    * "description": "lorem ipsum s dhsj hjs",
    * "payment_id": null,
    * "fan_id": 1,
    * "celebrity_id": 1,
    * "created_at": "2022-01-30T07:49:55.000000Z",
    * "updated_at": "2022-01-30T07:49:55.000000Z",
    * "event_date": null,
    * "type": null
    *}
     *],
    * "pivot": [
    *{
    * "celebrity": 1
    *},
    *{
    * "fan": 1
    *}
  *]
*}
*]
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=User::all();
        return response()->json($list);
    }

    public static function storeValidation(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'unique:users,phone',
            'username'=>'unique:users,username',
            'photo'=>'image',
            'email'=>'required|email|unique:users,email',
            'password'=>Rule::requiredIf($request->email!=null),
            'civility'=>['required',Rule::in(['m','mme'])]
        ]);
        return $validation;
    }

    /**
     * USER: Store a new user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=$this::storeValidation($request);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $file=ImageRepository::store($request,'photo','profil');
        $user=new User();
        if($file!=null){
            $user->photo=$file->uri;
        }
        $user->email=$request->email;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->civility=$request->civility;
        $user->enabled=true;
        if($request->password){
            $user->password=Hash::make($request->password);
        }
        $user->username=$request->username;
        $user->save();
        $token=$user->createToken('congo_live');
        $user->token=$token->plainTextToken;
        return response()->json($user);
    }

    /**
     * USER: Display the user by id.
     *
     * @response {
     * "id": 1,
     * "username": null,
     * "first_name": "hh",
     * "last_name": "massamba",
     * "photo": "https://storage.googleapis.com/congo-live.appspot.com/profil/font8zqejiuo.png?GoogleAccessId=firebase-adminsdk-s7bgq%40congo-live.iam.gserviceaccount.com&Expires=1646870400&Signature=FUNE4CN7CTzt0FuLxMW3r0yUR0YK3ZirQ2wTjWPvZ%2B1iwSqGmEHL9xDgMUCsuaftsV1RjJO2mD9Tq9JWc585lKvY1z8ji8y719E21vZ98imYykwEck3MQzktMEoDR7NDfu8QpGgAA70NF1eHpcd8uIzPBr9zzrK3yiV%2FFG4%2FrQ%2FUBQrhAIJt34uptBUe2lWdqEZjwCF3gTsW0dCfZEvyzeDoTZ05QaXF3xpscAGIvYAKR2Ddc1bxiqYDmGSrVQjvak8DGSO6DhE2TeLLoJDBEVQsuNW3cwIjYKW9ymwqGzyD48w%2Fb23HjAhHRjSIgNwPwQZQWdKlzR7WIiTqmIiDuQ%3D%3D",
     * "enabled": 1,
     * "civility": "m",
     * "email": null,
     * "email_verified_at": null,
     * "address": null,
     * "site_web": null,
     * "thumbnail": null,
     * "created_at": "2022-01-30T07:07:50.000000Z",
     * "updated_at": "2022-01-30T08:29:24.000000Z",
     * "phone": null,
     * "user": 1,
     * "categories": [],
     * "review": 0,
     * "posts": 18,
     * "autographs": [
     *{
    * "id": 1,
    * "amount": null,
    * "currency": null,
    * "state": "in_progress",
    * "description": "lorem ipsum s dhsj hjs",
    * "payment_id": null,
    * "fan_id": 1,
    * "celebrity_id": 1,
    * "created_at": "2022-01-30T07:49:55.000000Z",
    * "updated_at": "2022-01-30T07:49:55.000000Z",
    * "event_date": null,
    * "type": null
    *}
     *],
    * "pivot": [
    *{
    * "celebrity": 1
    *},
    *{
    * "fan": 1
    *}
  *]
*}
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=User::find($id);
        $item=$this->getModel($item);
        return response()->json($item);
    }

    /**
     * USER: Update user (celebity or fan).
     *
     * * @response {
     * "id": 1,
     * "username": null,
     * "first_name": "hh",
     * "last_name": "massamba",
     * "photo": "https://storage.googleapis.com/congo-live.appspot.com/profil/font8zqejiuo.png?GoogleAccessId=firebase-adminsdk-s7bgq%40congo-live.iam.gserviceaccount.com&Expires=1646870400&Signature=FUNE4CN7CTzt0FuLxMW3r0yUR0YK3ZirQ2wTjWPvZ%2B1iwSqGmEHL9xDgMUCsuaftsV1RjJO2mD9Tq9JWc585lKvY1z8ji8y719E21vZ98imYykwEck3MQzktMEoDR7NDfu8QpGgAA70NF1eHpcd8uIzPBr9zzrK3yiV%2FFG4%2FrQ%2FUBQrhAIJt34uptBUe2lWdqEZjwCF3gTsW0dCfZEvyzeDoTZ05QaXF3xpscAGIvYAKR2Ddc1bxiqYDmGSrVQjvak8DGSO6DhE2TeLLoJDBEVQsuNW3cwIjYKW9ymwqGzyD48w%2Fb23HjAhHRjSIgNwPwQZQWdKlzR7WIiTqmIiDuQ%3D%3D",
     * "enabled": 1,
     * "civility": "m",
     * "email": null,
     * "email_verified_at": null,
     * "address": null,
     * "site_web": null,
     * "thumbnail": null,
     * "created_at": "2022-01-30T07:07:50.000000Z",
     * "updated_at": "2022-01-30T08:29:24.000000Z",
     * "phone": null,
     * "user": 1,
     * "categories": [],
     * "review": 0,
     * "posts": 18,
     * "autographs": [
     *{
    * "id": 1,
    * "amount": null,
    * "currency": null,
    * "state": "in_progress",
    * "description": "lorem ipsum s dhsj hjs",
    * "payment_id": null,
    * "fan_id": 1,
    * "celebrity_id": 1,
    * "created_at": "2022-01-30T07:49:55.000000Z",
    * "updated_at": "2022-01-30T07:49:55.000000Z",
    * "event_date": null,
    * "type": null
    *}
     *],
    * "pivot": [
    *{
    * "celebrity": 1
    *},
    *{
    * "fan": 1
    *}
  *]
*}

     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new User());
        $validation=Validator::make($data,[
            'first_name'=>'min:1',
            'last_name'=>'min:1',
            'email'=>'unique:users,email,except,id|email',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        if ($request->password) {
            $data['password']=Hash::make($request->password);
        }
        $model=User::find($id);
        if($model==null){
            return response()->json($model,404);
        }
        $model->update($data);
        $model=$this->getModel($model);
        return response()->json($model);
    }

    /**
     * USER: Remove the user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * USER: Update the user (when you must update multimedia).
     *
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePoster(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new User());
        $validation=Validator::make($data,[
            'photo'=>'required|image',
            'first_name'=>'min:1',
            'last_name'=>'min:1',
            'email'=>'unique:users,email,except,id|email',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        $model=User::find($id);
        if($model==null){
            return response()->json($model,404);
        }
        //$file=ImageRepository::store($request,'file',"autographs");
        $info=$request->file('photo');
        $pathName=$info->getPathname();
        $file=fopen($pathName,'r');
        $extension=$info->getClientOriginalExtension();
        $key=Treatment::getPrivateImage(User::all(),"profil","profil/",".".$extension);
        $firebase=new FirebaseService();
        $firebase->setStorage($file,$key);
        $data['photo']=$key;
        $model->update($data);
        $model=$this->getModel($model);
        return response()->json($model);
    }

    public function getModel(User $model){
        $model->celebrity;
        $model->fan;
        $model->user=$model->user_id;
        if($model->photo){
            $firebase=new FirebaseService();
            $model->photo=$firebase->getUrl($model->photo);
        }
        /*if($celebrity!=null){
            $celebrityController=new CelebrityController();
            $item=$celebrityController->getModel($celebrity);
            $model->celebrity=$item;
        }else if($fan!=null){
            $fanController=new FanController();
            $item=$fanController->getModel($fan);
            $model->fan=$item;
        }*/
        return $model;
    }
}
