<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CelebrityCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * CATEGORY: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Category::all();
        foreach ($list as $item) {
            $item->items=$this->getModel($item);
            $item->makeHidden(['celebrities']);
        }
        return response()->json($list);
    }

    /**
     * CATEGORY: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'title'=>'required|unique:categories,title',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $model=new Category();
        $model->title=$request->title;
        $model->description=$request->description;
        $model->save();
        return response()->json($model);
    }

    /**
     * CATEGORY: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Category::find($id);
        if($item!=null){
            $item->items=$this->getModel($item);
            $item->makeHidden(['celebrities']);
            return response()->json($item);
        }else{
            return response()->json($item,404);
        }
    }

    /**
     * CATEGORY: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new Category());
        $validation=Validator::make($data,[
            'title'=>'min:1',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $model=Category::find($id);
        if($model==null){
            return response()->json(['error'=>'not found'],404);
        }
        $model->update($data);
        return response()->json($model);
    }

    /**
     * CATEGORY: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getModel(Category $category){
        $controller=new CelebrityController();
        $model=null;
        $celebrities=$category->celebrities;
        $items=[];
        foreach ($celebrities as $key => $value) {
            $items[]=$controller->getModel($value)->makeHidden(['categories']);
        }
        return $items;
    }
}
