<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * COMMENT: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Comment::all();
        $items=[];
        foreach ($list as $value) {
            $items[]=$this->getModel($value);
        }
        return response()->json($items);
    }

    /**
     * COMMENT: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'post'=>'required|exists:posts,id',
            'fan'=>'required|exists:fans,id',
            'content'=>'required'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $post=Post::find($request->post);
        $celebrity=$post->celebrity;
        $user=$celebrity->user;
        if($post->reviewable==false){
            return response()->json("This post can't have comments",400);
        }
        $model=new Comment();
        $model->post_id=$request->post;
        $model->fan_id=$request->fan;
        $model->content=$request->content;
        $model->save();
        $model=$this->getModel($model);

        $firebase=new FirebaseService();
        $firebase->createNotification(
            $request->user(),$user, "comment",
            "id",$request->content,"Nouveau commentaire"
        );
        return response()->json($model);
    }

    /**
     * COMMENT: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Comment::find($id);
        if($item!=null){
            $model=$this->getModel($item);
            return response()->json($model);
        }else{
            return response()->json($item,404);
        }
    }

    /**
     * COMMENT: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new Comment());
        $validation=Validator::make($data,[
            'content'=>'min:1',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        $model=Comment::find($id);
        if($model==null){
            return response()->json($model,404);
        }
        $model->update($data);
        return response()->json($model);
    }

    /**
     * COMMENT: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getModel(Comment $item){
        $model=$item;
        $model->post;
        $model->fan->user;
        return $model;
    }
}
