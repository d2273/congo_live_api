<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\CelebrityCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CelebrityCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * CELEBRITY AND CATEGORY: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'category'=>'required|exists:categories,id',
            'celebrity'=>'required|exists:celebrities,id',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $item=CelebrityCategory::where('celebrity_id',$request->celebrity)
            ->where('category_id',$request->category)->first();
        if($item==null){
            $model=new CelebrityCategory();
            $model->category_id=$request->category;
            $model->celebrity_id=$request->celebrity;
            $model->save();
            return response()->json($model);
        }else{
            $item->update(['enabled'=>true]);
            return response()->json($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new CelebrityCategory());
        $validation=Validator::make($data,[
            'enabled'=>'boolean',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        $model=CelebrityCategory::find($id);
        if($model==null){
            return response()->json($model,404);
        }
        $model->update($data);
        return response()->json($model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
