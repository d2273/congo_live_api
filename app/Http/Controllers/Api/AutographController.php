<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FirebaseService;
use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\Autograph;
use App\Models\Celebrity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AutographController extends Controller
{
    /**
     * AUTOGRPH: Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Autograph::all();
        $items=[];
        foreach ($list as $item) {
            $items[]=$this->getModel($item);
        }
        return response()->json($items);
    }

    /**
     * AUTOGRPH: Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'celebrity'=>'required|exists:celebrities,id',
            'fan'=>'required|exists:fans,id',
            'amount'=>'numeric',
            'event_date'=>'date'

        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        $celebrity=Celebrity::find($request->celebrity);
        $description=$request->description;
        $user=$celebrity->user;
        $model=new Autograph();
        $model->fan_id=$request->fan;
        $model->celebrity_id=$request->celebrity;
        $model->amount=$request->amount;
        $model->currency=$request->currency;
        $model->description=$request->description;
        $model->type=$request->type;
        $model->event_date=$request->event_date;
        $model->state='in_progress';
        $model->save();

        if($description===null){
            $description="Une nouvelle demande de dédicace envoyée.";
        }
        $firebase=new FirebaseService();
        $firebase->createNotification(
            $request->user(),$user, "comment",
            "id",$description,"Nouveau dédicace"
        );
        return response()->json($model);
    }

    /**
     * AUTOGRPH: Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item=Autograph::find($id);
        if($item!=null){
            $model=$this->getModel($item);
            return response()->json($model);
        }else{
            return response()->json($item,404);
        }

    }

    /**
     * AUTOGRPH: Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $data=ResponseHelpers::getFillableData($request,new Autograph());
        $validation=Validator::make($data,[
            'amount'=>'numeric',
            'currency'=>'min:1',
            'state'=>[Rule::in(['in_progress','pending','finished','rejected','canceled'])],
            'event_date'=>'date'
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if(count($data)==0){
            return response()->json("nothing to update",400);
        }
        $model=Autograph::find($id);
        $user=$request->user();
        if($request->state!=null){
            $celebrity=$model->celebrity;
            $fan=$model->fan;
            $state=$request->state;
            if('paided'===$model->state){
                unset($data['state']);
            }
            if($state === 'rejected' || $state === 'in_progress'){
                //for celebrity
                if($user->id!=$celebrity->id){
                    return response()->json([
                        "error"=>["celebrity"=>"This user is not the celebrity"]
                    ],400);
                }
            }else if ($state ==='canceled' || $state ==='finished'){
                if($user->id!=$fan->id){
                    return response()->json([
                        "error"=>["fan"=>"This user is not the fan"]
                    ],400);
                }
            }
        }
        if($model==null){
            return response()->json($model,404);
        }
        $model->update($data);
        return response()->json($model);
    }
    /**
     * AUTOGRPH: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getCelebrity(Autograph $item){
        $model=null;
        $celebrity=$item->celebrity;
        $user=$celebrity->user;
        if($user->photo!=null){
            $firebase=new FirebaseService();
            $user->photo=$firebase->getUrl($user->photo);
        }
        $model=$user;
        return $model;
    }

    public function getModel(Autograph $item){
        $firebase=new FirebaseService();
        $model=$item;
        foreach($model->files as $file){
            //$file->url=asset(Storage::url($file->file));
            $file->url=$firebase->getUrl($file->file);
        }
        $model->celebrity=$this->getCelebrity($item);
        $model->fan->user;
        return $model;
    }
}
