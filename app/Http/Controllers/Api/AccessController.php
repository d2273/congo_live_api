<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccessController extends Controller
{
    /**
     * LOGIN: Sign-in
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $validation=Validator::make($request->all(),[
            'email'=>'required|email|exists:users,email',
            'password'=>'required|min:8',
        ]);
        if($validation->fails()){
            return ResponseHelpers::getValidationResponse($validation);
        }
        if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])) {
            $user=Auth::user();
            $userController=new UserController();
            $user=$userController->getModel($user);
            $token=$request->user()->createToken('congo');
            $user->token=$token->plainTextToken;
            return response()->json($user);
        } else {
            return response()->json(['error'=>'not found'], 400);
        }
    }
}
