import { MoneySharp } from '@mui/icons-material';
import { Button, Grid, Input, InputAdornment, Paper, Typography } from '@mui/material';
import { isEmpty } from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import { Login } from './login';

function Example() {
    const[amount,setAmount]=React.useState("");
    const[value,setValue]=React.useState("");
    const[reference,setReference]=React.useState("");

    React.useEffect(()=>{
        const item=localStorage.getItem("congo-item");
        console.log('na bandi',item);
        fetch('http://localhost/congo_live/public/api/reference')
        .then(response=>{
            response.json().then(p=>{
                setReference(p);
            })
        })
    },[]);


    function handleSend(){
        if(isEmpty(amount)){
            alert("dssddsds");
            return false;
        }else{
            if(value>5000*100 || value<5*100){
                alert('cette somme');
            }
        }
    }
    function handleChange(v){
        setAmount(v);
        if(!isEmpty(v)){
            const som=parseFloat(v);
            console.log('som',som*100);
            setValue(som*100);
        }
    }
    return (
        <div style={{ height:'100vh',padding:0,margin:0,alignItems:'center',justifyContent:'center',display:'flex' }}>
            <Paper style={{ padding:10 }}>
                <Typography>
                    Vous vous préprez à payer
                </Typography>
                <Grid spacing={2} container>
                    <Grid item>
                        <Input
                            value={amount}
                            onChange={v=>handleChange(v.currentTarget.value)}
                            startAdornment={
                                <InputAdornment position='start'>
                                    $
                                </InputAdornment>
                            }
                        />
                    </Grid>
                    <Grid item>
                        <form action="https://api-testbed.maxicashapp.com/PayEntryPost" method="POST">
                            <input type="hidden" name="PayType" value="MaxiCash"/>
                            {/*<input type="text" name="Amount" id="amount" value="0"/>*/}
                            <Input value={value} name="Amount" type="hidden" />
                            <input type="hidden" name="Currency" value="MaxiDollar"/>
                            <input type="hidden" name="Telephone" value="+243823995810"/>
                            <input type="hidden" name="Email" value="henockmassamba@gmail.com"/>

                            <input type="hidden" name="MerchantID" value="65200e6ae5854df9b94b4c15e37604c5"/>
                            <input type="hidden" name="MerchantPassword" value="a34fe4f94fca42b896032bcff06aa409"/>
                            <input type="hidden" name="Language" value="Fr"/>
                            <input type="hidden" name="Reference" value={reference}  />
                            <input type="hidden" name="accepturl" value="http://localhost/congo_live/public/api/cash/accept"/>
                            <input type="hidden" name="cancelurl" value="http://localhost/congo_live/public/api/cash/cancel"/>
                            <input type="hidden" name="declineurl" value="http://localhost/congo_live/public/api/cash/decline"/>
                            <input type="hidden" name="notifyurl" value="http://localhost/congo_live/public/api/cash/notify"/>
                            <Button type='submit' onClick={handleSend}>
                                Envoyer
                            </Button>
                        </form>
                    </Grid>
                </Grid>
            </Paper>


        </div>
    );
}

export default Example;

if (document.getElementById('root')) {
    ReactDOM.render(<Example />, document.getElementById('root'));
}
