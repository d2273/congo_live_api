import { Box,Button,Input,Modal } from "@mui/material";
import React from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };



function Login(){

    const[visible,setVisible]=React.useState(false);

    function handleOpen(){
        setVisible(true);
    }

    function handleClose(){
        setVisible(false);
    }

    return(
        <React.Fragment>
            <Button onClick={handleOpen}>
                Envoyer
            </Button>
            <Modal
                open={visible}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Input
                        inputMode='email'
                    />
                </Box>
            </Modal>

        </React.Fragment>
    );
}

export {Login}
