<div class="overflow-hidden content-section" id="{{ $id }}">
    <h2>{{ $title }}</h2>
    <pre><code class="bash">

    </code></pre>
    <p>
        To get response you need to make a <strong>{{ $verb }}</strong> call to the following url :<br>
        <code class="higlighted break-word">{{ $url }}</code>
    </p>
    <br>
    <pre><code class="json">
        Result example :
        {{ $example }}
    </code></pre>
    <h4>QUERY PARAMETERS</h4>
    <table class="central-overflow-x">
        <thead>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($queries as $p)
            <tr>
                <td><code>{{ $p->field }}</code></td>
                <td><code>{{ $p->type }}</code></td>
                <td>{{ $p->description }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
