<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Congo live</title>
</head>
<body>
    @isset($amount,$auth_phone,$auth_email,$merchant,$password,$reference)
    <div class="container d-flex justify-content-center align-items-center" style="height: 100vh">
        <form action="https://api-testbed.maxicashapp.com/PayEntryPost" method="POST">
            <input type="hidden" name="PayType" value="MaxiCash">
            <input type="hidden" name="Amount" value="{{ $amount }}">
            <input type="hidden" name="Currency" value="MaxiDollar">
            <input type="hidden" name="Telephone" value="{{ $auth_phone }}">
            <input type="hidden" name="Email" value="{{ $auth_email }}">
            <input type="hidden" name="MerchantID" value="{{ $merchant }}">
            <input type="hidden" name="MerchantPassword" value="{{ $password }}">
            <input type="hidden" name="Language" value="Fr">
            <input type="hidden" name="Reference" value="{{ $reference }}">
            <input type="hidden" name="accepturl" value="{{ route('cash.accept') }}">
            <input type="hidden" name="cancelurl" value="{{ route('cash.cancel') }}">
            <input type="hidden" name="declineurl" value="{{ route('cash.decline') }}">
            <input type="hidden" name="notifyurl" value="{{ route('cash.notify') }}">
            <button class="btn btn-primary" type="submit">
                Commencer le paiement
            </button>
        </form>
    </div>
    @endisset
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
