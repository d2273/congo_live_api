<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Document</title>
</head>
<body>
    @isset($reference)
    @isset($item)
    <div class="body">
        <div class="container">
            <h1 style="font-size: 28px" class="center-align">
                Paiement de demande de dedicace
            </h1>
            <div>
                @if ($item->celebrity!=null)
                <img src="{{ $item->celebrity->photo }}" class="circle" alt="">
                @endif
            </div>
            <div class="row">
                <div class="col s12 m6 lg6 xl6">
                    @if ($item->celebrity!=null)
                        <h2 class="center-align" style="font-size: 20px;text-transform: uppercase;border-bottom: 1px solid teal;">
                            Célébrité
                        </h2>
                        <p style="font-size: 18px;font-weight: 700">
                            <span style="text-transform: capitalize">
                                {{ $item->celebrity->first_name }}
                            </span>
                            <span style="text-transform: uppercase">
                                {{ $item->celebrity->last_name }}
                            </span>
                        </p>
                    @endif
                </div>
                <div class="col s12 m6 lg6 xl6">
                    @if ($item->fan!=null)
                        <h2 class="center-align" style="font-size: 20px;text-transform: uppercase;border-bottom: 1px solid teal;">
                            Demandeur
                        </h2>
                        <p style="font-size: 18px;font-weight: 700">
                            <span>
                                <img src="{{ $item->fan->user->photo }}" class="circle" alt="">
                            </span>
                            <span style="text-transform: capitalize">
                                {{ $item->fan->user->first_name }}
                            </span>
                            <span style="text-transform: uppercase">
                                {{ $item->fan->user->last_name }}
                            </span>
                        </p>
                    @endif
                </div>
            </div>
            <div>
                {{ $item->description }}
            </div>
            <div>
                <form action="https://api-testbed.maxicashapp.com/PayEntryPost" method="POST">
                    <input type="hidden" name="PayType" value="MaxiCash"/>
                    <Input value="{{ ($item->amount*100) }}" id="amount" name="Amount" type="hidden" />
                    <input type="hidden" name="Currency" value="MaxiDollar"/>
                    <input type="hidden" name="Telephone" value="+243823995810"/>
                    <input type="hidden" name="Email" value="henockmassamba@gmail.com"/>

                    <input type="hidden" name="MerchantID" value="65200e6ae5854df9b94b4c15e37604c5"/>
                    <input type="hidden" name="MerchantPassword" value="a34fe4f94fca42b896032bcff06aa409"/>
                    <input type="hidden" name="Language" value="Fr"/>
                    <input type="hidden" name="Reference" value="{{ $reference }}"  />
                    <input type="hidden" name="accepturl" value="{{ route('cash.accept') }}"/>
                    <input type="hidden" name="cancelurl" value="{{ route('cash.cancel') }}"/>
                    <input type="hidden" name="declineurl" value="{{ route('cash.decline') }}"/>
                    <input type="hidden" name="notifyurl" value="{{ route('cash.notify') }}"/>
                    <Button class="waves-effect waves-light btn" type='submit' onClick={handleSend}>
                        Payer {{ $item->amount. ' '. $item->currency }}
                    </Button>
                </form>
            </div>
        </div>
    </div>
    @endisset
    @endisset
    <style>
        .body{
            height: 100vh;
            align-items: center;
            justify-content: center;
            justify-items: center;
            display: flex;
        }
        .container{
            display: flex;
            align-items: center;
            flex-direction: column;
        }
    </style>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>
