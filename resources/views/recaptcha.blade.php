<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</head>
<body>
    <form id="myform" method="POST" >
        <input name="field" id="field" required type="text"></input>
        <div class="g-recaptcha" data-sitekey="6LeD0JkdAAAAAMy5xYddHUCvQrUL3-vYTd0rVh7X"></div>
        <input type="button" onclick="checkRecaptcha();" value="submit"></input>
    </form>
</body>
<script>
    function checkRecaptcha() {
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
          });
          $( "#myform" ).validate({
            rules: {
              field: {
                required: true
              }
            }
          });
      var response = grecaptcha.getResponse();
      if(response.length == 0) {
        //reCaptcha not verified
        alert("no pass");
      }
      else {
        //reCaptch verified
        alert("pass");
      }
    }

    // implement on the backend
    function backend_API_challenge() {
        var response = grecaptcha.getResponse();
        $.ajax({
            type: "POST",
            url: 'https://www.google.com/recaptcha/api/siteverify',
            data: {"secret" : "6LeD0JkdAAAAAMV-6S1_cEizAYiu4CzSciKGUjsm", "response" : response, "remoteip":"localhost"},
            contentType: 'application/x-www-form-urlencoded',
            success: function(data) { console.log(data); }
        });
    }
    </script>
</html>
